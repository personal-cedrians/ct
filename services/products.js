import axios from 'axios';

const URL_BASE = 'https://ctsite.herokuapp.com'

export  async function getProducts(search, category){
    const respuesta = await axios.get(URL_BASE + '/api/products', {
        params: {
            search,
            category
        }
    })
    return respuesta.data;
}