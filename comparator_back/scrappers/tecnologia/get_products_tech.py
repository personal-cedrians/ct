import requests
import json
import pathlib
from bs4 import BeautifulSoup
import os
import asyncio
import aiohttp
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

class GetProductsTech():

    async def YaEsta(self, search):
        URL = 'https://www.yaesta.com/productos?search='+search
        chrome_options = webdriver.FirefoxOptions()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.binary_location = os.environ.get("FIREFOX_BIN")
        browser = webdriver.Firefox(executable_path=os.environ.get("GECKODRIVER_PATH"), firefox_options=chrome_options)
        browser.get(URL)
        await asyncio.sleep(0);
        content = BeautifulSoup(browser.page_source, 'html.parser')
        results = content.find_all('div', class_='product-inner')
        productos = []
        try:
            for result in results:
                nombres = result.find('h3')
                precio = result.find('ins')
                if(not precio):
                    precio = result.find('b')
                img_url = 'https://www.yaesta.com'+result.find('img')['src']
                link = 'https://www.yaesta.com'+result.find('a')['href']
                producto = {
                    'nombres': nombres.text,
                    'precio': precio.text,
                    'imagen': img_url,
                    'link': link
                }
                productos.append(producto)
            respuesta = {
                "tienda": "yaesta.com",
                "url": URL,
                "productos": productos
            }
        except Exception as e:
            print(e);
            respuesta = {
                "tienda": "yaesta.com",
                "url": URL,
                "productos": []
            }
        return respuesta

    async def LaGanga(self, search):
        URL = 'https://www.almaceneslaganga.com/pedidos-en-linea/?search='+search
        async with aiohttp.ClientSession() as session:
            async with session.get(URL) as resp:
                page = await resp.read()
                content = BeautifulSoup(page, 'html.parser')
                try:
                    results = content.find_all('div', class_='esquema_producto')
                    productos = []
                    credito = 0
                    for result in results:
                        credito = 0
                        nombres = result.find('div', class_="marca_producto_interno").text + ' ' + result.find('label',{'style': 'font-size: 10px;height: 16px;overflow-x: hidden;overflow-y: hidden;width: 100%;margin-bottom: 0px;'}).text
                        precio = result.find('span', class_='precio_producto')
                        if(not precio):
                            credito = 1
                            precio = result.find('span', class_='letra_pequeña letra_tipo_credito')
                        img_url = result.find('img')['src']
                        if credito == 0:
                            link = 'https://www.almaceneslaganga.com/pedidos-en-linea/efectivo/'+result.find('button', class_='btn btn-primary btn-detalles')['producto']
                        else:
                            link = 'https://www.almaceneslaganga.com/pedidos-en-linea/credito/'+result.find('button', class_='btn btn_agregar_carrito_catalogo btn_amarillo')['producto']
                        producto = {
                            'nombres': nombres,
                            'precio': precio.text,
                            'imagen': img_url,
                            'link': link
                        }
                        productos.append(producto)
                    respuesta = {
                        "tienda": "La Ganga",
                        "url": URL,
                        "productos": productos
                    }
                except:
                    respuesta = {
                        "tienda": "La Ganga",
                        "url": URL,
                        "productos": []
                    }
                
                return respuesta

    async def Comandato(self, search):
        URL = 'https://www.comandato.com/'+search
        async with aiohttp.ClientSession() as session:
            async with session.get(URL) as resp:
                page = await resp.read()
                content = BeautifulSoup(page, 'html.parser')
                try:
                    results = content.find_all('div', class_='producto')
                    productos = []
                    for result in results:
                        nombres = result.find('a')['title']
                        precio = result.find('span', class_='pwebvalor nuevoiva')
                        if(not precio):
                            precio = result.find('div', class_='pnormal nuevoiva')
                        if(not precio):
                            precio = result.find('div', class_='price')
                        img_url = result.find('img')['src']
                        link = result.find('a')['href']
                        producto = {
                            'nombres': nombres,
                            'precio': precio.text,
                            'imagen': img_url,
                            'link': link
                        }
                        productos.append(producto)
                    respuesta = {
                        "tienda": "Comandato",
                        "url": URL,
                        "productos": productos
                    }
                except Exception as e:
                    print(e)
                    respuesta = {
                        "tienda": "Comandato",
                        "url": URL,
                        "productos": []
                    }
                
                return respuesta

    async def Artfacta(self, search):
        URL = 'https://www.artefacta.com/catalogsearch/result/?q='+search
        async with aiohttp.ClientSession() as session:
            async with session.get(URL) as resp:
                page = await resp.read()
                content = BeautifulSoup(page, 'html.parser')
                try:
                    results = content.find_all('div', class_='product-item-info')
                    productos = []
                    for result in results:
                        nombres = result.find('strong', class_='product name product-item-name')
                        precio= result.find('span', class_='special-price')
                        if(not precio):
                            precio = result.find('span', class_='price')
                        img_url = result.find('img')['src']
                        link = result.find('a')['href']
                        producto = {
                            'nombres': nombres.text.strip(),
                            'precio': precio.text,
                            'imagen': img_url,
                            'link': link
                        }
                        productos.append(producto)
                    respuesta = {
                        "tienda": "Artefacta",
                        "url": URL,
                        "productos": productos
                    }
                except Exception as e:
                    print(e)
                    respuesta = {
                        "tienda": "Artefacta",
                        "url": URL,
                        "productos": []
                    }
                
                return respuesta

    async def Novicompu(self, search):
        URL = 'https://www.novicompu.com/busqueda?controller=search&search_query='+search
        async with aiohttp.ClientSession() as session:
            async with session.get(URL) as resp:
                page = await resp.read()
                content = BeautifulSoup(page, 'html.parser')
                try:
                    results = content.find_all('div', class_='list-item col-lg-2 col-md-6 col-sm-6 col-xs-12')
                    productos = []
                    for result in results:
                        nombres = result.find('div', class_='product-name')
                        precio= result.find('div', class_='content_price')
                        img_url = result.find('img')['data-src']
                        link = result.find('a')['href']
                        producto = {
                            'nombres': nombres.text.strip(),
                            'precio': precio.text,
                            'imagen': img_url,
                            'link': link
                        }
                        productos.append(producto)
                    respuesta = {
                        "tienda": "Novicompu",
                        "url": URL,
                        "productos": productos
                    }
                except Exception as e:
                    print(e)
                    respuesta = {
                        "tienda": "Novicompu",
                        "url": URL,
                        "productos": []
                    }
                
                return respuesta