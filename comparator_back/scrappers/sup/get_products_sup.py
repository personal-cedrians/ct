import requests
import json
import pathlib
from bs4 import BeautifulSoup
import os
import asyncio
import aiohttp
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class GetProductsSupermercado():

    async def ElEstabloSup(self, search):
        URL = 'https://www.comisariatoelestablo.com/?product_cat=&s='+search+'&post_type=product'
        async with aiohttp.ClientSession() as session:
            async with session.get(URL) as resp:
                page = await resp.read()
                content = BeautifulSoup(page, 'html.parser')
                try:
                    results = content.find_all('div', class_='product-small box')
                    productos = []
                    for result in results:
                        nombres = result.find('p', class_="name product-title")
                        precio = result.find('ins')
                        if(not precio):
                            precio = result.find('span', class_='price')
                        img_url = result.find('img')['src']
                        link = result.find('a')['href']
                        producto = {
                            'nombres': nombres.text,
                            'precio': precio.text,
                            'imagen': img_url,
                            'link': link
                        }
                        productos.append(producto)
                    respuesta = {
                        "tienda": "El Establo Supermercado",
                        "url": URL,
                        "productos": productos
                    }
                except:
                    respuesta = {
                        "tienda": "El Establo Supermercado",
                        "url": URL,
                        "productos": []
                    }
                
                return respuesta

    async def CoralHiper(self, search):
        URL = 'https://www.coralhipermercados.com/buscar?q='+search+'~'
        chrome_options = webdriver.FirefoxOptions()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.binary_location = os.environ.get("FIREFOX_BIN")
        browser = webdriver.Firefox(executable_path=os.environ.get("GECKODRIVER_PATH"), firefox_options=chrome_options)
        browser.get(URL)
        await asyncio.sleep(0);
        content = BeautifulSoup(browser.page_source, 'html.parser')
        results = content.find_all('div', class_='wrapperDetalle')
        productos=[]
        try:
            for result in results:
                nombres = result.find('p', class_="nomProdCarrito")
                precio = result.find('ins')
                if(not precio):
                    precio = result.find('div', class_='preProductoCar')
                img_url = result.find('img')['src']
                link = result.find('a')['href']
                producto = {
                    'nombres': nombres.text,
                    'precio': precio.text,
                    'imagen': img_url,
                    'link': 'https://www.coralhipermercados.com'+link
                }
                productos.append(producto)
            browser.quit()
            respuesta = {
                "tienda": "Coral Hipermercados",
                "url": URL,
                "productos": productos
            }
        except Exception as e:
            print(e)
            respuesta = {
                "tienda": "Coral Hipermercados",
                "url": URL,
                "productos": []
            }
        return respuesta

    async def WanllaSupermercado(self, search):
        URL = 'https://www.wanlla.com/shop?search='+search
        async with aiohttp.ClientSession() as session:
            async with session.get(URL) as resp:
                page = await resp.read()
                content = BeautifulSoup(page, 'html.parser')
                try:
                    results = content.find_all('div', class_='col-md-4 oe_product oe_grid oe_product_cart oe-height-4')
                    productos = []
                    for result in results:
                        nombres = result.find('h5')
                        precio = result.find('ins')
                        if(not precio):
                            precio = result.find('span', class_='oe_currency_value')
                        img_url = result.find('img')['src']
                        link = result.find('a', itemprop="name")['href']
                        producto = {
                            'nombres': nombres.text,
                            'precio': precio.text,
                            'imagen': 'https://www.wanlla.com'+img_url,
                            'link': 'https://www.wanlla.com'+link
                        }
                        productos.append(producto)
                    respuesta = {
                        "tienda": "Wanlla Supermercado",
                        "url": URL,
                        "productos": productos
                    }
                except:
                    respuesta = {
                        "tienda": "Wanlla Supermercado",
                        "url": URL,
                        "productos": []
                    }

                return respuesta
    
    async def Frecuento(self, search):
        URL = 'https://www.frecuento.com/frecuento/es/search/?text='+search
        async with aiohttp.ClientSession() as session:
            async with session.get(URL) as resp:
                page = await resp.read()
                content = BeautifulSoup(page, 'html.parser')
                try:
                    results = content.find_all('li', class_='product__list--item col-xs-12 col-sm-12 col-md-3 clearfix col-lg-3')
                    productos = []
                    for result in results:
                        nombres = result.findNext('a', class_='product__list--name')
                        precio = result.findNext('div', class_= 'product__listing--price')
                        if(not precio):
                            precio = {'text': 'Sin precio'}
                        img_url = result.find('img')['src']
                        link = result.find('a')['href']
                        producto = {
                            'nombres': nombres.text,
                            'precio': precio.text,
                            'imagen': img_url,
                            'link': 'https://www.frecuento.com'+link
                        }
                        productos.append(producto)
                    respuesta = {
                        "tienda": "Frecuento",
                        "url": URL,
                        "productos": productos
                    }
                except:
                    respuesta = {
                        "tienda": "Frecuento",
                        "url": URL,
                        "productos": []
                    }
                
                return respuesta

    async def Disgralec(self, search):
        URL = 'http://www.disgralec.com/index.php?fc=module&module=leoproductsearch&controller=productsearch&orderby=position&orderway=desc&cate=&search_query='+search
        async with aiohttp.ClientSession() as session:
            async with session.get(URL) as resp:
                page = await resp.read()
                content = BeautifulSoup(page, 'html.parser')
                try:
                    results = content.find_all('div', class_='product-container product-block text-center')
                    productos = []
                    for result in results:
                        nombres = result.find('a')['title']
                        precio = result.find('span', class_='price product-price')
                        if(not precio):
                            precio = result.find('span', class_='price product-price')
                        img_url = result.find('img')['src']
                        link = result.find('a', itemprop="name")['href']
                        producto = {
                            'nombres': nombres,
                            'precio': precio.text,
                            'imagen': 'https://www.wanlla.com'+img_url,
                            'link': 'https://www.wanlla.com'+link
                        }
                        productos.append(producto)
                    respuesta = {
                        "tienda": "Disgralec",
                        "url": URL,
                        "productos": productos
                    }
                except:
                    respuesta = {
                        "tienda": "Disgralec",
                        "url": URL,
                        "productos": []
                    }

                return respuesta