import requests
import json
from bs4 import BeautifulSoup
import asyncio
import aiohttp
import re


class GetProductsFerreteria():

    async def KyviScrap(self, search):
        URL = 'https://kywitiendaenlinea.com/?s='+search+'&post_type=product'
        async with aiohttp.ClientSession() as session:
            async with session.get(URL) as resp:
                page = await resp.read()
                content = BeautifulSoup(page, 'html.parser')
                try:
                    results = content.find_all('div', class_='product-inner')
                    productos = []
                    for result in results:
                        nombres = result.find('h3')
                        precio = result.find('ins')
                        if(not precio):
                            precio = result.find('span', class_='price')
                        img_url = result.find('img')['src']
                        link = result.find('a')['href']
                        producto = {
                            'nombres': nombres.text,
                            'precio': precio.text,
                            'imagen': img_url,
                            'link': link
                        } 
                        productos.append(producto)
                    respuesta = {
                        "tienda": "Kywi",
                        "url": URL,
                        "productos": productos
                    }
                except:
                    respuesta = {
                        "tienda": "Kywi",
                        "url": URL,
                        "productos": []
                    }

                
                return (respuesta)
            
    async def DePrati(self, search):
        URL = 'https://www.deprati.com.ec/search?text='+search
        async with aiohttp.ClientSession() as session:
            async with session.get(URL) as resp:
                page = await resp.read()
                content = BeautifulSoup(page, 'html.parser')
                try:
                    array = content.find('product-grid', class_='product--grid--component')['rawresults']
                    results = json.loads(array)
                    productos = []
                    for result in results:
                        nombres = result['name'].replace('<em class="search-results-highlight">', "").replace("</em>"," ")
                        precio = result['price']['formattedValue']
                        if(not precio):
                            precio = result['price']['value']
                        img_url = result['images'][0]['url']
                        link = result['url']
                        producto = {
                            'nombres': nombres,
                            'precio': precio,
                            'imagen': img_url,
                            'link': 'https://www.deprati.com.ec'+link
                        } 
                        productos.append(producto)
                    respuesta = {
                        "tienda": "DePrati",
                        "url": URL,
                        "productos": productos
                    }
                except Exception as e:
                    print(e)
                    respuesta = {
                        "tienda": "DePrati",
                        "url": URL,
                        "productos": []
                    }

                
                return (respuesta)

    async def Pycca(self, search):
        URL = 'https://www.pycca.com/'+search
        async with aiohttp.ClientSession() as session:
            async with session.get(URL) as resp:
                page = await resp.read()
                content = BeautifulSoup(page, 'html.parser')
                try:
                    results = content.find_all('div', class_='productVitrine')
                    productos = []
                    for result in results:
                        nombres = result.find('h4')
                        precio = result.find('div', class_='Vitrina Agotado ')
                        if(not precio):
                            script = result.find('script').string
                            p = re.findall('var priceA = (.*);', script)
                            precio = p[0].replace("'","")
                        img_url = result.find('img')['src']
                        link = result.find('a')['href']
                        producto = {
                            'nombres': nombres.text.strip(),
                            'precio': precio,
                            'imagen': img_url,
                            'link': link
                        } 
                        productos.append(producto)
                    respuesta = {
                        "tienda": "Pycca",
                        "url": URL,
                        "productos": productos
                    }
                except Exception as e:
                    print(e)
                    respuesta = {
                        "tienda": "Pycca",
                        "url": URL,
                        "productos": []
                    }

                
                return (respuesta)