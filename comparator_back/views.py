from django.shortcuts import render
import json
import random

from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework import status

from .scrappers.ferreteria.get_products_fer import GetProductsFerreteria
from .scrappers.sup.get_products_sup import GetProductsSupermercado
from .scrappers.tecnologia.get_products_tech import GetProductsTech
import asyncio

# Create your views here.

@api_view(['GET'])
def prueba_comparator(request):
    data = request.GET
    respuesta = []
    if(data['category'] == 'S'):
        supRequester = GetProductsSupermercado()
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        # coral = loop.create_task(supRequester.CoralHiper(data['search']))
        elestablo = loop.create_task(supRequester.ElEstabloSup(data['search']))
        wanlla = supRequester.WanllaSupermercado(data['search'])
        frecuento = loop.create_task(supRequester.Frecuento(data['search']))
        # disgralec = loop.create_task(supRequester.Disgralec(data['search']))
        gathered = asyncio.gather(*[ wanlla, elestablo, frecuento])
        respuesta = loop.run_until_complete(gathered)
        loop.close()
        random.shuffle(respuesta)
        return Response(respuesta)

    if(data['category'] == 'H'):
        supRequester = GetProductsFerreteria()
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        kywi = loop.create_task(supRequester.KyviScrap(data['search']))
        pycca = loop.create_task(supRequester.Pycca(data['search']))
        dePrati = loop.create_task(supRequester.DePrati(data['search']))
        gathered = asyncio.gather(*[kywi, pycca, dePrati])
        respuesta = loop.run_until_complete(gathered)
        loop.close()
        random.shuffle(respuesta)
        return Response(respuesta)

    if(data['category'] == 'T'):
        supRequester = GetProductsTech()
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        comandato = loop.create_task(supRequester.Comandato(data['search']))
        # yaesta = loop.create_task(supRequester.YaEsta(data['search']))
        laganga = loop.create_task(supRequester.LaGanga(data['search']))
        artefacta = loop.create_task(supRequester.Artfacta(data['search']))
        novicompu = loop.create_task(supRequester.Novicompu(data['search']))
        gathered = asyncio.gather(*[novicompu, artefacta, comandato, laganga])
        respuesta = loop.run_until_complete(gathered)
        loop.close()
        random.shuffle(respuesta)
        return Response(respuesta)
