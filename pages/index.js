import ReactGA from 'react-ga';
import SearchForm from "../components/SearchForm";
import { Container, Row, Col } from "react-bootstrap";
import StoreList from "../components/StoreList";
import { useState, useEffect } from 'react';
import { getProducts } from "../services/products";
import Router from 'next/router'
import BlockUi from 'react-block-ui';
import Footer from 'rc-footer';
import Header from "../components/Header";
import { useRouter } from 'next/router'
import HeadInfo from "../components/HeadInfo";

ReactGA.initialize('UA-170119757-1', {
  titleCase: false,
  testMode: true,
  gaOptions: {
    name: 'trackerCata'
  }
});

export default function Index(props) {
  const [usedSearch, setUsedSearch] = useState(false)
  const [results, setResult] = useState([])
  const [error, setError] = useState(false)
  const [loaded, setLoaded] = useState(false)
  const router = useRouter()
  const {category, search} = router.query;
  const { stores } = props;
  
  
  useEffect(() => {
    async function fetchData(){
      if(category && search){
        setLoaded(true)
        let stores;
        try{
          stores = await getProducts(search, category);
        }catch(e){
          setLoaded(false)
          setError(true)
        }
        if(stores){
          setResult(stores)
          ReactGA.event({
            category: 'User',
            action: 'Busqueda',
            label: `${category} - ${search}`,
          });
          setUsedSearch(true)
          ReactGA.pageview(`/?category=${category}&search=${search}`);
          setError(false)
        }else{
          setUsedSearch(false)
          setError(false)
        }
        setLoaded(false)
      }
    }
    fetchData();
  },[search, category]);

  const _renderResults = () => {
  return results.length === 0
      ? <p>No se encontraron resultados</p>
      : (<div>
      <p>Los establecimientos aparecen en orden <strong>aleatorio</strong> en cada búsqueda</p>
      <StoreList stores={results} />
      </div>)

  }


  return (
    <div
    style={{
      height:'100%'
    }}
    >
    <HeadInfo search={search} category={category}/>
    <BlockUi tag="div" blocking={loaded} >
    <Container fluid>
      <Row>
        <Col md={12}>
          <Header
          style={{
            pading:"1rem",
          }}
          ></Header>
        </Col>
      </Row>
        <Row>
            <Col md={2}></Col>
            <Col>
            <SearchForm />
            </Col>
            <Col md={2}></Col>
        </Row>
        <Row>
        <Col md={2}></Col>
            <Col>
            {
            error ?
            <p>Hubo un <strong>Error</strong> intente de nuevo mas tarde</p>:
            usedSearch
            ? _renderResults()
        : <p>Busque un producto para empezar</p>}
            
            </Col>
            <Col md={2}></Col>
        </Row>
    </Container>
    </BlockUi>
    <Footer
    className={'foo'}
    theme={'dark'}
    columns={[
      {
        title: (
          <div>
          <p>Todas las demás marcas comerciales y marcas comerciales registradas son propiedad de sus respectivos propietarios.</p>
          </div>
        ),
      },
    ]}
    bottom={(
      <div>
      <p>
     Contacto: <a></a>
      </p>
      </div>
    )}
  />,
</div>
  )
}
