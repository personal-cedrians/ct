import 'bootstrap/dist/css/bootstrap.min.css';
import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import 'react-block-ui/style.css';
import 'react-multi-carousel/lib/styles.css';
import 'rc-footer/assets/index.css';
import './styles.css'
 
function MyApp({ Component, pageProps }) {
 
  return <Component {...pageProps} />;
 
}
 
export default MyApp;