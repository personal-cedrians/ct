import Head from 'next/head'

const HeadInfo = (props) => {

    const { search, category } = props;

    const _putHead = () => {
        if (search && category) {
            return (
                <div>
                    <Head>
                        <link rel="icon" type="image/x-icon" href="/favicon.ico" />
                        <title>{search} - CATATODO</title>
                        <meta name="title" content={search + ' - CATATODO'} key="title"/>
                        <meta name="description" 
                        content={"Encontrará su busqueda: " + search + " en varias tiendas online, Catatodo es un catálogo de productos en diferentes tiendas virtuales."} 
                        key="description"/>
                        <meta name="keywords" content={search + ' - CATATODO'} key="keywords"/>
                        <meta name="robots" content="index, nofollow"/> 
                        <meta charSet="utf-8"/>
                        <meta property="og:title" key="og:title" content="How to Become an SEO Expert (8 Steps)" />
                        <meta property="og:description" 
                        key="og:description"
                        content={"Encontrará su busqueda: " + search + " en varias tiendas online, Catatodo es un catálogo de productos en diferentes tiendas virtuales."} />
                        <meta property="og:image"
                         key="og:image"
                         content="/favicon.png" />
                    </Head>
                </div>
            )
        } else {
            return (
                <div>
                    <Head>
                    <link rel="icon" type="image/x-icon" href="/favicon.ico" />
                        <title>CATATODO - Busque productos en varias tiendas</title>
                        <meta name="title" content="CATATODO" key="title"/>
                        <meta name="description" 
                        content="Busque un producto en varias tiendas online, Catatodo es un catálogo de productos en diferentes tiendas virtuales." 
                        key="description"/>
                        <meta name="keywords" content="Supermercado, online, tecnologia, productos" key="keywords"/>
                        <meta name="robots" content="index, follow"/> 
                        <meta charSet="utf-8"/>
                        <meta name="keywords" content="CATATODO" key="keywords"/>
                        <meta property="og:description" 
                        key="og:description"
                        content="Busque un producto en varias tiendas online, Catatodo es un catálogo de productos en diferentes tiendas virtuales."/>
                        <meta property="og:image"
                         key="og:image"
                         content="/favicon.png" />
                    </Head>
                </div>
            )
        }

    }

    return (
        <div>
            {_putHead()}
        </div>
    )
}

export default HeadInfo;