import { Button, Row, Col } from 'react-bootstrap'
import {SelectButton} from 'primereact/selectbutton';

import { useState } from 'react';
import { useRouter } from 'next/router'
import { FaStore } from "react-icons/fa";
import { FaHome } from "react-icons/fa";
import { FaMousePointer } from "react-icons/fa";

const SearchForm = (props) => {
  const [search, setSearch] = useState('')
  const [category, setCategory] = useState('')
  const router = useRouter()

  const optionTemplate = (option) => {

    return (
      <div>
        {option.icon()}
        <p>{option.label}</p>
      </div>
    )
}

  
  const categorySelectItems = [
    {label: 'Supermercado', value: 'S', icon:() => {
      return <FaStore
      style={{
        fontSize: '200%'
      }}/>
    },
  className: 'selectB'
  },
    {label: 'Tecnología', value: 'T', icon:() => {
      return <FaMousePointer
      style={{
        fontSize: '200%'
      }}/>
    },
  className: 'selectB'
  },
    {label: 'Hogar', value: 'H', icon:() => {
      return <FaHome
      style={{
        fontSize: '200%'
      }}/>
    },
    className: 'selectB'
  },
];

  const _handleChange = (e) => {
    setSearch(e.target.value.trim())
  }

  const _handleSubmit = (e) => {
    e.preventDefault()
    router.push(`/?category=${category}&search=${search}`, undefined, {})
  }

  const _handleChangeCategory = (e) => {
    setCategory(e.value)
  }

    return (
      <form onSubmit={_handleSubmit}>
        <Row>
          <Col md={12}>
          <SelectButton 
          style={{
            display: 'flex',
            justifyContent: 'space-around',
            paddingBottom: '1rem',
            backgroundColor: 'white'
          }}
          value={category} 
          options={categorySelectItems} 
          onChange={_handleChangeCategory}
          itemTemplate={optionTemplate}></SelectButton>
          </Col>
        </Row>
        <Row between="xs">
            <Col xs={8}>
            <input
              className="input btn-block"
              onChange={_handleChange}
              type="text"
              placeholder="Busca tu artículo..."
              disabled={!category} />
            </Col>
            <Col xs={4}>
            <Button 
            onClick={_handleSubmit} 
            className="btn-block" 
            variant="dark" 
            disabled={!category || !search}>Search</Button>
            </Col>
        </Row>
      </form>
    )
}

export async function getData(search, category){
        const url = `${API_URL}/api/products/`
        return await axios.get(url, {
            params: {
                search,
                category
            }
        })
    }
export default SearchForm;