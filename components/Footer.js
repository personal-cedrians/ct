const Footer = () =>{
    return (
        <footer>
            <h5>Hecho con ♥</h5>
            <p>Todas las demás marcas comerciales y marcas comerciales registradas son propiedad de sus respectivos propietarios.</p>
            <p>Si quieres que aparezca un nuevo establecimiento o desea que su establecimiento no aparezca por favor pongase en contacto a este correo electrónico</p>
        </footer>
    )
}

export default Footer;