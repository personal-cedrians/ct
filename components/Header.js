const Header = () => {
    return (
        <header style={{
            display: 'flex',
            justifyContent: 'center',
            fontFamily: 'TimesNewRoman'
        }}>
            <h1>CATATODO</h1>
        </header>
    )
}

export default Header;