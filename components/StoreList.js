import PropTypes from 'prop-types'
import { Card } from 'primereact/card';
import { Row, Col, Button } from 'react-bootstrap';
import Carousel from "react-multi-carousel";
import './StoreList.module.css';
import ReactGA from 'react-ga';

const StoreList = (props) => {
    const responsive = {
        desktop: {
          breakpoint: { max: 3000, min: 1024 },
          items: 3,
          slidesToSlide: 3,
          partialVisibilityGutter: 30
        },
        tablet: {
          breakpoint: { max: 1024, min: 464 },
          items: 2,
          slidesToSlide: 2,
          partialVisibilityGutter: 30
        },
        mobile: {
          breakpoint: { max: 464, min: 0 },
          items: 1,
          slidesToSlide: 1,
          partialVisibilityGutter: 30
        }
      };
    const productTemplate = (product, key, tienda) =>{
        return (
            <div key={key} style={{   
                overflow: 'hidden',
                padding: '10px',
                display: 'flex',
                height: '100%',
                flex: '0 1 30%',
                borderStyle: 'double',
                }}>
                <Row>
                    <Col md={12}>
                    <ReactGA.OutboundLink
                    eventLabel={tienda}
                    to={product.link}
                    target="_blank"
                    trackerNames={['trackerCata']}
                    >
                        <img style={{width: '75%', height: '75%'}}  src={product.imagen} alt={product.nombres} />
                    </ReactGA.OutboundLink>
                    </Col>
                    <Col md={12}>
                    <p>{product.nombres} </p>
                    </Col>
                    <Col md={12}>
                    <span><b>{product.precio}</b></span>
                    </Col>
                    <Col md={12}>
                    <ReactGA.OutboundLink
                    eventLabel={tienda}
                    to={product.link}
                    target="_blank"
                    trackerNames={['trackerCata']}
                    >
                    <Button className="btn btn-block" variant="outline-dark">
                        <span>Ver producto en tienda</span>
                    </Button>
                    </ReactGA.OutboundLink>
                    </Col>
                </Row>
            </div>
        )
    }
    
    


        const { stores } = props
        return (
            <div>
                    {stores.map((store, index)=>{
                        return (
                            <Row key={index}>
                                <Col >
                                    <Card title={store.tienda} subTitle={
                                        <a 
                                        href={store.url} 
                                        target="_blank"
                                        rel="noopener noreferrer">Ver más en la tienda</a>
                                        }>
                                            {
                                                store.productos.length > 0 ? 
                                                <Carousel
                                                swipeable={true}
                                                draggable={false}
                                                responsive={responsive}
                                                // ssr={true}
                                                infinite={true}
                                                keyBoardControl={true}
                                                customTransition="transform 300ms ease-in-out"
                                                removeArrowOnDeviceType={["mobile"]}
                                                partialVisible={true}
                                                showDots={true}
                                                minimumTouchDrag={50}
                                                containerClass="first-carousel-container container"
                                                focusOnSelect={true}
                                              >
                                                  {store.productos.map((product, index) => productTemplate(product, index, store.tienda))}

                                              </Carousel>
                                                : <span>No se encontraron productos</span>
                                            }
                                        </Card>
                                </Col>
                            </Row>
                        )
                    })}
            </div>
        )
}


export default StoreList;