const withCSS = require('@zeit/next-css');
const withPlugins = require('next-compose-plugins');

const nextConfig = {
  webpack: function(config) {
    config.module.rules.push({
      test: /\.(eot|woff|woff2|ttf|svg|png|jpg|gif)$/,
      use: {
        loader: 'url-loader',
        options: {
          limit: 100000,
          name: '[name].[ext]'
        }
      }
    },
    {
      test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
      issuer: {
        test: /\.jsx?$/
      },
      use: ['babel-loader', '@svgr/webpack', 'url-loader']
    });
    return config;
  },
  exportPathMap: async function (
    defaultPathMap,
    { dev, dir, outDir, distDir, buildId }
  ) {
    return {
      '/': { page: '/'},
    }
  },
};

module.exports = withPlugins([withCSS, {cssModules: true}], nextConfig);